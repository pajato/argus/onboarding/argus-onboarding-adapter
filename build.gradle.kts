plugins {
    id("com.pajato.plugins.pajato-convention-plugin")
}

group = "com.pajato.argus"
version = "0.9.1"
description = "The Argus adapter layer onboarding feature project"

koverReport { filters { excludes { classes(listOf("**.*\$\$serializer")) } } }

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.+")
    implementation("com.pajato.argus:argus-onboarding-core:0.9.+")
    implementation("com.pajato.argus:argus-network-core:0.9.+")
    implementation("com.pajato.argus:argus-profile-core:0.9.+")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("com.pajato:pajato-test:0.9.+")
}

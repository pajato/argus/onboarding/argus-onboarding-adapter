# argus-onboarding-adapter

## Description

This project implements Argus core interfaces for the Onboarding feature.
It exists to provide an implementation of those interfaces used by the Use Case layer.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Started, see Versions.md for the current version.

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

For specific information see the
[core layer documentation](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CoreLayer.md).

The single responsibility for this project is to provide the interface implementations used by the
Use Case architectural layer for the Onboarding feature.

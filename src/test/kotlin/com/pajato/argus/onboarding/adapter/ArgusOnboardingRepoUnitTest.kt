package com.pajato.argus.onboarding.adapter

import com.pajato.argus.network.core.Network
import com.pajato.argus.onboarding.core.OnboardingError
import com.pajato.argus.profile.core.Profile
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import java.io.File
import kotlin.io.path.toPath
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

private const val INJECT_FAIL_MESSAGE = "Unexpected failure to inject the onboarding resource file!"
class ArgusOnboardingRepoUnitTest {
    private val classLoader = this::class.java.classLoader

    @Test fun `When injecting a null, verify onboarding is null`() {
        runBlocking { ArgusOnboardingRepo.inject(null) }
        assertEquals(null, ArgusOnboardingRepo.onboarding)
    }

    @Test fun `When injecting a non-existent file, verify it gets created with default values`() {
        val uri = classLoader.getResource("files/empty.txt")?.toURI() ?: fail(INJECT_FAIL_MESSAGE)

        fun assert() {
            val onboarding = ArgusOnboardingRepo.onboarding ?: fail(INJECT_FAIL_MESSAGE)
            assertEquals(false, onboarding.hasOnboarded)
            assertEquals("", onboarding.tmdbApiKey)
            assertEquals(0, Json.decodeFromString<List<Network>>(onboarding.networkListAsJson).size)
            assertEquals(0, Json.decodeFromString<List<Profile>>(onboarding.profileListAsJson).size)
        }

        uri.toPath().toFile().delete()
        runBlocking { ArgusOnboardingRepo.inject(uri) }
        assert()
    }

    @Test fun `When injecting a valid URI, verify onboarding is correct`() {
        val uri = classLoader.getResource("files/onboarding.txt")?.toURI() ?: fail(INJECT_FAIL_MESSAGE)

        fun assert() {
            val onboarding = ArgusOnboardingRepo.onboarding ?: fail(INJECT_FAIL_MESSAGE)
            assertEquals(true, onboarding.hasOnboarded)
            assertEquals("xyzzy", onboarding.tmdbApiKey)
            assertEquals(3, Json.decodeFromString<List<Network>>(onboarding.networkListAsJson).size)
            assertEquals(1, Json.decodeFromString<List<Profile>>(onboarding.profileListAsJson).size)
        }

        runBlocking { ArgusOnboardingRepo.inject(uri) }
        assert()
    }

    @Test fun `When injecting from an invalid (empty) file, verify an onboarding error`() {
        val uri = classLoader.getResource("files/empty.txt")?.toURI() ?: fail(INJECT_FAIL_MESSAGE)

        suspend fun assert() {
            assertFailsWith<OnboardingError> { ArgusOnboardingRepo.inject(uri) }.also {
                assertEquals("Invalid JSON: \"\"", it.message)
            }
        }

        uri.toPath().toFile().writeText("")
        runBlocking { assert() }
    }

    @Test fun `When persisting a null onboarding instance, verify an onboarding error`() {
        suspend fun assert() {
            assertFailsWith<OnboardingError> { ArgusOnboardingRepo.persist() }.also {
                assertEquals("OnboardingInjectionError", it.message)
            }
        }

        runBlocking {
            ArgusOnboardingRepo.inject(null)
            assert()
        }
    }

    @Test fun `When persisting a default onboarding instance, verify the persisted file!`() {
        val uri = classLoader.getResource("files/persist.txt")?.toURI() ?: fail(INJECT_FAIL_MESSAGE)

        suspend fun assert() {
            val file: File = uri.toPath().toFile().also { it.delete() }
            assertEquals(0, file.length())
            ArgusOnboardingRepo.persist()
            assertEquals("{}", file.readText())
        }

        runBlocking {
            ArgusOnboardingRepo.inject(uri)
            assert()
        }
    }
}

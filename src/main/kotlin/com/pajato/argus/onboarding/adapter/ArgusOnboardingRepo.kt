package com.pajato.argus.onboarding.adapter

import com.pajato.argus.onboarding.core.I18nStrings
import com.pajato.argus.onboarding.core.Onboarding
import com.pajato.argus.onboarding.core.OnboardingError
import com.pajato.argus.onboarding.core.OnboardingRepo
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.lang.Exception
import java.net.URI
import kotlin.io.path.toPath

object ArgusOnboardingRepo : OnboardingRepo {
    private const val JSON = """{"hasOnboarded":false,"tmdbApiKey":"","networkListAsJson":"[]","profileListAsJson":"[]"}"""
    private var uri: URI? = null
    override var onboarding: Onboarding? = null

    override suspend fun inject(uri: URI?) {
        @Throws(OnboardingError::class)
        fun decode(uri: URI): Onboarding {
            val json = uri.toPath().toFile().readText()
            fun getMessage() = "Invalid JSON: \"$json\""
            return try { Json.decodeFromString(json) } catch (exc: Exception) { throw OnboardingError(getMessage()) }
        }

        fun setupFileMaybe(file: File) { if (!file.exists()) file.writeText(JSON) }

        ArgusOnboardingRepo.uri = uri
        if (uri != null) setupFileMaybe(uri.toPath().toFile())
        onboarding = if (uri != null) decode(uri) else null
    }

    override suspend fun persist() {
        val uri = uri ?: throw OnboardingError(I18nStrings.ONBOARDING_INJECTION_ERROR)
        uri.toPath().toFile().writeText(Json.encodeToString(onboarding))
    }
}

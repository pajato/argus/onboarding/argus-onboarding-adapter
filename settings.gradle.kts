@file:Suppress("UnstableApiUsage")

rootProject.name = "argus-onboarding-adapter"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        mavenLocal()
    }

    plugins {
        id("com.pajato.plugins.pajato-convention-plugin") version "0.9.+"
    }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        mavenLocal()
    }
}
